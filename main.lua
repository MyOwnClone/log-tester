require('alien')

function FileExists(filename)
   local f=io.open(filename,"r")
   if f~=nil 
        then io.close(f) 
        return true 
    else 
        return false 
    end
end

function GetNumberOfValidLuaLines(filename)
    local f=io.open(filename,"r")    
    
    local counter = 0
    
    for line in f:lines() do 
        local lineLength = #line 
        
        if (lineLength > 0) then
            counter = counter + 1
        end
    end
    io.close(f)
    
    return counter
end

function GetNumberOfCLines(filename)
    local dll = alien.load("binaryfileconverter-dll.dll");
    local func = dll.GetNumberOfCLines 
    func:types("int", "string")
    
    lineCount = func(filename)
    
    return lineCount
end

local prefix = "kav"
prefix = prefix.."_"
local suffix = "log"

local pathPrefix = "..\\logs\\"

local counter = 0

local differenceCounter = 0
local differenceCount = 0

for i=0,200 do
    local filename = pathPrefix..prefix..counter.."."..suffix
    if (FileExists(filename)) then
        local numberOfValidLuaLines = GetNumberOfValidLuaLines(filename)
        local numberOfCLines = GetNumberOfCLines(filename)
        
        if (numberOfValidLuaLines ~= numberOfCLines) then
            local difference = numberOfCLines - numberOfValidLuaLines
            print(filename.. ": number of valid lua lines: "..numberOfValidLuaLines.." number of C lines: "..numberOfCLines..", difference: "..difference)
            
            differenceCount = differenceCount + difference
            differenceCounter = differenceCounter+1
        end
    end
    
    counter = counter+1
end 

print("Number of invalid files: "..differenceCounter)
print("Number of lines potentially ignored by Lua: "..differenceCount)

os.execute("pause")